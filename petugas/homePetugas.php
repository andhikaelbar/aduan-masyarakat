<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <title>Halaman Home</title>
</head>
<body class="wrapper">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a href="#" class="navbar-brand">Pengaduan</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarAtas" aria-controls="navbarAtas" aria-expanded="false"  aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

            <div class="collapse navbar-collapse" id="navbarAtas">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="#" class="nav-link active me-auto mb-2 mb-lg-0">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="pengaduan.php" class="nav-link">Pengaduan</a>
                    </li>
                    <li class="nav-item">
                        <a href="tanggapan.php" class="nav-link">Tanggapan</a>
                    </li>
                    <li class="nav-item">
                        <a href="konfirmasi.php" class="nav-link">Konfirmasi</a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan.php" class="nav-link">Laporan</a>
                    </li>
                </ul>
                    <form class="d-flex">
                        <a href="../login.php" class="btn btn-outline-danger" type="submit">Log Out</a>
                    </form>
            </div>
        </div>
    </nav>

    <div class="mt-5">
        <center><h1>Youkoso, Ninggen-domo</h1></center>
    </div>

    <script src="../assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>