<?php include '../config/pengaturan.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <title>Halaman Login Petugas</title>
</head>
<body class="wrapper container" style="display: flex; flex-wrap: wrap; justify-content: space-around;">
    <div class="card col-5 mt-5" style="box-shadow: 0 10px 10px rgba(0,0,0,0.2);">
        <div class="card-header"><center><h1>Login Petugas</h1></center></div>
        <div class="card-body">
            <?php if (isset($_GET['pesan'])) { ?>
                <div class="alert alert-warning mb-2 mt-2"><?=$_GET['pesan']?></div>
            <?php } ?>
            <form action="../proses/cekLogin.php" method="POST" class="form-login">
                <label for="username">Username : </label>
                <input type="text" name="username" id="username" autofocus class="form-control mb-3">
                <label for="password">Password : </label>
                <input type="password" name="password" id="password" class="form-control mb-3">
                <input type="submit" value="Login" class="btn btn-primary col-12">
            </form>
        </div>
    </div>
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>