<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <title>Halaman Daftar</title>
</head>

<body class="wrapper container" style="display: flex; flex-wrap: wrap; justify-content: space-around;">
    <div class="card col-6 mt-5" style="box-shadow: 0 10px 10px rgba(0,0,0,0.2);">
        <div class="card-header">
            <center>
                <h1>Daftar</h1>
            </center>
        </div>
        <div class="card-body">
            <form action="../proses/daftar.php" method="POST" class="form-register">
                <div class="row mb-3">
                    <label for="nik" class="col-3">NIK : </label>
                    <div class="col-9">
                        <input type="text" name="nik" id="nik" class="form-control" autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="nama" class="col-3">Nama : </label>
                    <div class="col-9">
                        <input type="text" name="nama" id="nama" class="form-control">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="username" class="col-3">Username : </label>
                    <div class="col-9">
                        <input type="text" name="username" id="username" class="form-control" autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="hp" class="col-3">No Telp : </label>
                    <div class="col-9">
                        <input type="number" name="hp" id="hp" class="form-control" autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="password" class="col-3">Password : </label>
                    <div class="col-9">
                        <input type="password" name="password" id="password" class="form-control" autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="konf" class="col-3">Konfirmasi : </label>
                    <div class="col-9">
                        <input type="password" name="konf" id="konf" class="form-control" autofocus>
                    </div>
                </div>
                <input type="submit" value="Daftar" class="btn btn-primary col-12">
            </form>
        </div>
    </div>
    <script src="../assets/js/bootstrap.min.js"></script>
</body>

</html>