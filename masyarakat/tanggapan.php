<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <title>Halaman Home</title>
</head>
<body class="wrapper">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a href="#" class="navbar-brand">Pengaduan</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarAtas" aria-controls="navbarAtas" aria-expanded="false"  aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarAtas">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="homeMasyarakat.php" class="nav-link me-auto mb-2 mb-lg-0">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="pengaduan.php" class="nav-link">Pengaduan</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link active">Tanggapan</a>
                    </li>
                    <li class="nav-item">
                        <a href="konfirmasi.php" class="nav-link">Konfirmasi</a>
                    </li>
                    <li class="nav-item">
                        <a href="laporan.php" class="nav-link">Laporan</a>
                    </li>
                </ul>
                    <form class="d-flex">
                        <a href="../login.php" class="btn btn-outline-danger" type="submit">Log Out</a>
                    </form>
            </div>
        </div>
    </nav>
    <div class="container mt-5">
        <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    Accordion Item #1
                </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
                </div>
            </div>
        </div>
    </div>
    <script>
    </script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>