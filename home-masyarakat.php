<?php include 'config/pengaturan.php'; ?>
<?php include 'proses/FilterMasyarakat.php'; ?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Halaman Pengadoean Masyarakat</title>
</head>
<body class="wrapper">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a href="#" class="navbar-brand">Pengaduan</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarAtas" aria-controls="navbarAtas" aria-expanded="false"  aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarAtas">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="home.php" class="nav-link me-auto mb-2 mb-lg-0">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link active">Pengaduan</a>
                    </li>
                    <li class="nav-item">
                        <a href="tanggapan.php" class="nav-link">Tanggapan</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $base_url ?>proses/Logout.php" class="nav-link text-danger">Logout</a>
                    </li>   
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="container mt-3 mb-5 bg-light pengaduan">
        <h1>Laporan Form</h1>
        <hr>
        <form action="proses/kirimPengaduan.php" method="POST" enctype="multipart/form-data">
            <label for="gambar">Gambar : </label>
            <input type="file" name="gambar" id="gambar" class="form-control mb-3">
            <label for="isi">Isi : </label>
            <textarea name="isi" required id="isi" class="form-control mb-3" rows="10"></textarea>
            <input type="submit" value="KIRIM" class="btn btn-primary">
        </form>
        <hr>
        <div>
            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        Cras justo odio
                    </div>
                    <span class="badge bg-primary rounded-pill">14</span>
                </li>
            </ul>
        </div>
    </div>
    <script>
    </script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>